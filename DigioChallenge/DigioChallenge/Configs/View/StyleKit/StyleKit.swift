//
//  StyleKit.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 30/03/21.
//

import UIKit

struct StyleKit {

    public struct fonts {
        
        static let smallText = UIFont(name: "Arial-Regular", size: 10.0)
        static let normalText = UIFont(name: "Arial-Regular", size: 12.0)
        static let titleText = UIFont(name: "Arial-Regular", size: 14.0)
        
        static let boldText = UIFont(name: "Arial-BoldMT", size: 12.0)
        static let boldMediumText = UIFont(name: "Arial-BoldMT", size: 16.0)
        static let boldLargeText = UIFont(name: "Arial-BoldMT", size: 20.0)
    }
    
    public struct colors {
        
        static let white = UIColor.white
        static let black = UIColor.black
        static let gray = UIColor(red: 0.2666336298, green: 0.2666856647, blue: 0.2666303515, alpha: 1)
        static let blue = UIColor(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1)
    }
    
    public struct borders {
    
        static let views = CGFloat(24.0)
    }
}

