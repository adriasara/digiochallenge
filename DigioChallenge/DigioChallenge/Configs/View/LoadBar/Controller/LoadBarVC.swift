//
//  LoadBarVC.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 30/03/21.
//

import UIKit

final class LoadBarVC: UIViewController {
    
    let loadBarView: LoadBarView = LoadBarView(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.sv(loadBarView)
        loadBarView.fillContainer()
    }
}
