//
//  ProductsDetailView.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 31/03/21.
//

import UIKit
import Stevia
import Alamofire
import AlamofireImage

final class ProductsDetailView: UIView {
    
    private lazy var whiteView: UIView = {
        let whiteView = UIView(frame: .zero)
        whiteView.backgroundColor = .white
        whiteView.translatesAutoresizingMaskIntoConstraints = false
        return whiteView
    }()
    
    private lazy var image: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.layer.cornerRadius = StyleKit.borders.views
        image.clipsToBounds = true
        image.contentMode = .scaleToFill
        return image
    }()
    
    private lazy var label: UILabel = {
        var label = UILabel(frame: .zero)
        label.font = StyleKit.fonts.normalText
        label.backgroundColor = .white
        label.textColor = StyleKit.colors.black
        label.textAlignment = .justified
        label.numberOfLines = 0
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }

    private func commonInit() {
        backgroundColor = .white
        subviews()
        layout()
    }

    private func subviews() {

        whiteView.sv([image, label])
        
        sv([whiteView])
    }

    private func layout() {
        
        whiteView.left(0).top(0).right(0).bottom(0)

        image.left(10).right(10).top(20).height(150)
        
        label.left(10).right(10).Top == image.Bottom + 20
    }
    
    func setValues(tag: Int, row: Int, model: ProductsModel) {
        
        if tag == 0 {
            if let urlImage = model.spotlight?[row].bannerURL {
                AF.request(urlImage).responseImage { response in
                    if case .success(let image) = response.result {
                        self.image.image = image
                    }
                }
            }
            label.text = model.spotlight?[row].descriptionSpotlight
        } else if tag == 1 {
            if let urlImage = model.cash?.bannerURL {
                AF.request(urlImage).responseImage { response in
                    if case .success(let image) = response.result {
                        self.image.image = image
                    }
                }
            }
            label.text = model.cash?.descriptionCash
        } else if tag == 2 {
            if let urlImage = model.products?[row].imageURL {
                AF.request(urlImage).responseImage { response in
                    if case .success(let image) = response.result {
                        self.image.contentMode = .center
                        self.image.image = image
                    }
                }
            }
            label.text = model.products?[row].descriptionProducts
        }
    }
    
    func setupLayout(_ view: UIView) {
        whiteView.setupConstrainsLayout(view)
    }
}
