//
//  ProductsDetailVC.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 31/03/21.
//

import UIKit

final class ProductsDetailVC: UIViewController {
    
    let productsDetailView: ProductsDetailView = ProductsDetailView(frame: .zero)
    var tbTag: Int = 0
    var clvRow: Int = 0
    var productsModel = ProductsModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.sv([productsDetailView])
        productsDetailView.setupLayout(self.view)
        view.backgroundColor = .white
        productsDetailView.fillContainer()

        productsDetailView.setValues(tag: tbTag, row: clvRow, model: productsModel)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if tbTag == 0 {
            if let name = productsModel.spotlight?[clvRow].name {
                title = name
            }
        } else if tbTag == 1 {
            if let name = productsModel.cash?.title {
                title = name
            }
        } else if tbTag == 2 {
            if let name = productsModel.products?[clvRow].name {
                title = name
            }
        }
    }
}
