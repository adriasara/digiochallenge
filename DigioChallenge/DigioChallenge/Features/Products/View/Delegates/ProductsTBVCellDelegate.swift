//
//  ProductsTBVCellDelegate.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 31/03/21.
//

import UIKit

extension ProductsView: UITableViewDelegate {
    
}

extension ProductsView: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductsTBVCell.reusableIdentifier, for: indexPath) as? ProductsTBVCell else { return UITableViewCell() }
        
        cell.productsModel = productsModel
        cell.tbTag = indexPath.section
        cell.delegate = self
        
        if indexPath.section == 2 {
            cell.setupCollection()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return sectionsTitle[section]
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView = UIView(frame: .zero)
        headerView.backgroundColor = .white

        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = StyleKit.fonts.boldLargeText
        headerLabel.textColor = StyleKit.colors.blue
        headerLabel.text = sectionsTitle[section]
        headerLabel.sizeToFit()

        if section == 1 {
            headerLabel.attributedText = sectionsTitle[section].changeLabelFont(secondWord: "Cash")
        }

        headerView.addSubview(headerLabel)

        return headerView
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 2 {
            return 150
        }
        return 250
    }
}

extension ProductsView: ProductsTBCellDelegate {
    
    func selectProduct(tbTag: Int, clvRow: Int, model: ProductsModel) {
        
        delegate?.selectProduct(tbTag: tbTag, clvRow: clvRow, model: model)
    }
}


