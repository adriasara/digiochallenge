//
//  ProductsCLVCellDelegate.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 31/03/21.
//

import UIKit

extension ProductsTBVCell: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        delegate?.selectProduct(tbTag: tbTag, clvRow: indexPath.row, model: productsModel ?? ProductsModel())
    }
}

extension ProductsTBVCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if tbTag == 0 {
            return productsModel?.spotlight?.count ?? 0
        } else if tbTag == 1 {
            return 1
        } else if tbTag == 2 {
            return productsModel?.products?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductsCLVCell.reusableIdentifier, for: indexPath) as? ProductsCLVCell else { return UICollectionViewCell() }
    
        cell.setModel(tag: tbTag, indexPath: indexPath, productsModel ?? ProductsModel())
        
        if tbTag == 2 {
            cell.setupLastSection()
        }
        
        return cell
    }
}

extension ProductsTBVCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if tbTag == 2 {
            return CGSize(width: 100, height: 100)
        }
        return CGSize(width: 500, height: 220)
    }
}
