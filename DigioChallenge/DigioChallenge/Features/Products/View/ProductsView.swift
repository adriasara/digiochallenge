//
//  ProductsView.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 30/03/21.
//

import UIKit
import Stevia

protocol ProductsViewDelegate: NSObjectProtocol {
    func selectProduct(tbTag: Int, clvRow: Int, model: ProductsModel)
}

final class ProductsView: UIView {
    
    weak var delegate: ProductsViewDelegate?
    
    var productsModel = ProductsModel()
    let sectionsTitle = ["", "digio Cash", "products".localized()]
    
    private lazy var whiteView: UIView = {
        let whiteView = UIView(frame: .zero)
        whiteView.backgroundColor = .white
        whiteView.translatesAutoresizingMaskIntoConstraints = false
        return whiteView
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.backgroundColor = .white
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var logoImage: UIImageView = {
        let logoImage = UIImageView(frame: .zero)
        logoImage.image = UIImage(named: "logo")
        return logoImage
    }()
    
    private lazy var titleHome: UILabel = {
        let titleHome = UILabel(frame: .zero)
        titleHome.text = "hello".localized() + " Maria"
        titleHome.font = StyleKit.fonts.boldMediumText
        titleHome.textColor = StyleKit.colors.gray
        titleHome.textAlignment = .left
        return titleHome
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.register(ProductsTBVCell.self, forCellReuseIdentifier: ProductsTBVCell.reusableIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        tableView.isPagingEnabled = false
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.isUserInteractionEnabled = true
        tableView.allowsSelectionDuringEditing = false
        tableView.allowsSelection = false
        tableView.backgroundColor = .clear
        tableView.bounces = false
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        backgroundColor = .white
        subviews()
        layout()
    }
    
    private func subviews() {
    
        stackView.addArrangedSubview(logoImage)
        stackView.addArrangedSubview(titleHome)
        
        whiteView.sv([stackView, tableView])
        
        sv([whiteView])
    }
    
    private func layout() {
        
        whiteView.left(0).top(0).right(0).bottom(0)
                
        stackView.top(20).left(20).right(20)
        
        logoImage.width(20).heightEqualsWidth().left(0).centerVertically().centerHorizontally()
        
        titleHome.centerVertically().centerHorizontally().Left == logoImage.Right + 10
        
        tableView.left(20).right(20).bottom(20).Top == stackView.Bottom + 5
    }
    
    func setModel(_ model: ProductsModel) {
        productsModel = model
        tableView.reloadData()
    }
    
    func setupLayout(_ view: UIView) {
        whiteView.setupConstrainsLayout(view)
    }
}
