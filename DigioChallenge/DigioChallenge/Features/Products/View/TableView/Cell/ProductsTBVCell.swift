//
//  ProductsTBVCell.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 31/03/21.
//

import UIKit
import Stevia

protocol ProductsTBCellDelegate: NSObjectProtocol {
    func selectProduct(tbTag: Int, clvRow: Int, model: ProductsModel)
}

final class ProductsTBVCell: UITableViewCell {
        
    var productsModel: ProductsModel?
    var tbTag: Int = 0
    
    weak var delegate: ProductsTBCellDelegate?
    
    private lazy var flowLayout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
        return flowLayout
    }()

    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.register(ProductsCLVCell.self, forCellWithReuseIdentifier: ProductsCLVCell.reusableIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isScrollEnabled = true
        collectionView.backgroundColor = .clear
        collectionView.clipsToBounds = true
        collectionView.bounces = false
        return collectionView
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
      
    private func commonInit() {
        backgroundColor = .white
        subviews()
        layout()
    }
    
    private func subviews() {

        sv([collectionView])
    }
    
    private func layout() {
        
        collectionView.left(0).right(0).top(10).bottom(20)
    }
    
    func setupCollection() {
        collectionView.left(20)
    }
}

extension ProductsTBVCell {
    class var reusableIdentifier: String {
        return String(describing: self)
    }
}
