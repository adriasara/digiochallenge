//
//  ProductsVC.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 30/03/21.
//

import UIKit

final class ProductsVC: UIViewController {
    
    let productsView: ProductsView = ProductsView(frame: .zero)
    var productsViewModel: ProductsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        requestProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = "home".localized()
    }
    
    private func requestProducts() {
        
        productsViewModel = ProductsViewModel(model: .init(), viewController: self)
        
        productsViewModel?.requestProducts(completion: { (response) in
            
            if let e = response as? NSError {
                self.setSnackBarText(e.localizedDescription)
            } else if let server_response = response as? Server_Response {
                if let mssg = server_response.mssg {
                    self.setSnackBarText(mssg)
                }
            } else {
                if let jResult = response as? ProductsModel {
                    
                    self.productsView.delegate = self
                    self.view.sv([self.productsView])
                    self.productsView.setupLayout(self.view)
                    self.productsView.fillContainer()
                    self.productsView.productsModel = jResult
                }
            }
        })
    }
}

extension ProductsVC: ProductsViewDelegate {
    
    func selectProduct(tbTag: Int, clvRow: Int, model: ProductsModel) {
        
        let vc = ProductsDetailVC()
        vc.tbTag = tbTag
        vc.clvRow = clvRow
        vc.productsModel = model
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
