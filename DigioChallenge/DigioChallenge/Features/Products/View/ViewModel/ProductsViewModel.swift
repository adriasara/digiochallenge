//
//  ProductsViewModel.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 30/03/21.
//

import UIKit

final class ProductsViewModel: NSObject {
    
    private let model: ProductsModel
    private let viewController: UIViewController
    
    init(model: ProductsModel = .init(), viewController: UIViewController) {
        self.model = model
        self.viewController = viewController
    }
    
    func requestProducts(completion: ((_ object: Any) -> Void)?) {
        
        self.service(loading: false, controller: viewController, operation: .products, type: ProductsModel.self) { (response) in
            
            if let error = response as? NSError {
                completion?(error)
            } else if let server_response = response as? Server_Response {
                completion?(server_response)
            } else {
                completion?(response)
            }
        }
    }
}
