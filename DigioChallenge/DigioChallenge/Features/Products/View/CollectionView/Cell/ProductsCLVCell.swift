//
//  ProductsCLVCell.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 30/03/21.
//

import UIKit
import Stevia
import Alamofire
import AlamofireImage

final class ProductsCLVCell: UICollectionViewCell {
        
    private lazy var whiteView: UIView = {
        let whiteView = UIView(frame: .zero)
        whiteView.backgroundColor = .white
        whiteView.layer.cornerRadius = StyleKit.borders.views
        whiteView.translatesAutoresizingMaskIntoConstraints = false
        return whiteView
    }()

    private lazy var homeImage: UIImageView = {
        let homeImage = UIImageView(frame: .zero)
        homeImage.layer.cornerRadius = StyleKit.borders.views
        homeImage.clipsToBounds = true
        homeImage.contentMode = .scaleToFill
        return homeImage
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }

    private func commonInit() {
        backgroundColor = .white
        subviews()
        layout()
    }

    private func subviews() {
        
        sv([whiteView, homeImage])
    }

    private func layout() {

        whiteView.left(2).right(0).top(0).bottom(0)

        homeImage.left(2).right(0).top(0).bottom(0)
    }
    
    func setupLastSection() {
        whiteView.layer.shadowRadius = 5
        whiteView.layer.shadowOffset = .zero
        whiteView.layer.shadowColor = UIColor(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1).cgColor
        whiteView.layer.shadowOpacity = 0.5
    }

    func setModel(tag: Int, indexPath: IndexPath, _ model: ProductsModel) {
        if tag == 0 {
            if let urlImage = model.spotlight?[indexPath.row].bannerURL {
                AF.request(urlImage).responseImage { response in
                    if case .success(let image) = response.result {
                        self.homeImage.image = image
                    }
                }
            }
        } else if tag == 1 {
            if let urlImage = model.cash?.bannerURL {
                AF.request(urlImage).responseImage { response in
                    if case .success(let image) = response.result {
                        self.homeImage.image = image
                    }
                }
            }
        } else if tag == 2 {
            if let urlImage = model.products?[indexPath.row].imageURL {
                AF.request(urlImage).responseImage { response in
                    if case .success(let image) = response.result {
                        self.homeImage.image = image
                    }
                }
            }
        }
    }
}

extension ProductsCLVCell {
    class var reusableIdentifier: String {
        return String(describing: self)
    }
}
