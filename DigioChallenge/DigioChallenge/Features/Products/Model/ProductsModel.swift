//
//  ProductsModel.swift
//  DigioChallenge
//
//  Created by Ádria Cardoso on 30/03/21.
//

import UIKit

final class ProductsModel: Codable {
    
    var spotlight: [Spotlight]?
    var products: [Products]?
    var cash: Cash?
    
    private enum CodingKeys: String, CodingKey {
        
        case spotlight
        case products
        case cash
    }
    
    init() {
        
    }
    
    required init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        spotlight = try? container.decode([Spotlight].self, forKey: .spotlight)
        products = try? container.decode([Products].self, forKey: .products)
        cash = try? container.decode(Cash.self, forKey: .cash)
    }
}

struct Spotlight: Codable {
    
    var name: String?
    var bannerURL: String?
    var descriptionSpotlight: String?
    
    private enum CodingKeys: String, CodingKey {
        
        case name
        case bannerURL
        case descriptionSpotlight = "description"
    }
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        name = try? container.decode(String.self, forKey: .name)
        bannerURL = try? container.decode(String.self, forKey: .bannerURL)
        descriptionSpotlight = try? container.decode(String.self, forKey: .descriptionSpotlight)
    }
}

struct Products: Codable {
    
    var name: String?
    var imageURL: String?
    var descriptionProducts: String?
    
    private enum CodingKeys: String, CodingKey {
        
        case name
        case imageURL
        case descriptionProducts = "description"
    }
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        name = try? container.decode(String.self, forKey: .name)
        imageURL = try? container.decode(String.self, forKey: .imageURL)
        descriptionProducts = try? container.decode(String.self, forKey: .descriptionProducts)
    }
}

struct Cash: Codable {
    
    var title: String?
    var bannerURL: String?
    var descriptionCash: String?
    
    private enum CodingKeys: String, CodingKey {
        
        case title
        case bannerURL
        case descriptionCash = "description"
    }
    
    init() {
        
    }
    
    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)

        title = try? container.decode(String.self, forKey: .title)
        bannerURL = try? container.decode(String.self, forKey: .bannerURL)
        descriptionCash = try? container.decode(String.self, forKey: .descriptionCash)
    }
}


