## DigioChallenge

First of all, I would like to thank you to give me this opportunite. 

### Installation

It's necessary access the project directory by cmd and run the following command:

+ pod install


### Organization

The project was organized by the MVVM (Model-View-ViewModel) design pattern. 


### Libs 

+ Alamofire: for rest API requests;
+ MaterialComponents: for snackBar design;
+ SteviaLayout: for constraint layout;
+ AlamofireImage: for imageURL download.
